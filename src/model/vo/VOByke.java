package model.vo;

import java.util.Date;

/**
 * Representation of a byke object
 */
public class VOByke implements Comparable<VOByke>{
	private String id;
	private String name;
	private String city;
	private String latitude;
	private String longitude;
	private String dpcapacity;
	private String only_date;
	
	public VOByke(String pId, String pName,String pCity, String pLat, String pLong, String pCap, String pDate)
	{
		id=pId;
		name=pName;
		city=pCity;
		latitude= pLat;
		longitude=pLong;
		dpcapacity=pCap;
		only_date=pDate;
	}
	/**
	 * @return id_bike - Bike_id
	 */
	public String darId() 
	{
		return id;
	}
	public String darName() 
	{
		return name;
	}	
	public String darCity() 
	{
		return city;
	}	
	public String darLatitude() 
	{
		return latitude;
	}	
	public String darLongitude() 
	{
		return longitude;
	}	
	public String darDpcapacity() 
	{
		return dpcapacity;
	}	
	public String darOnlyDate() 
	{ 
		return only_date;
	}
	@Override
	public int compareTo(VOByke arg0) {
		// TODO Auto-generated method stub
		return 0;
	}	
}
