package model.vo;

/**
 * Representation of a Trip object
 */
public class VOTrip implements Comparable <VOTrip>
{
	private String trip_id;
	private String start_time;
	private String end_time;
	private String bike_id;
	private String tripDiuration;
	private String fromStationId;
	private String fromStationName;
	private String toStationId;
	private String toStationName;
	private String userType;
	private String gender;
	private String birthYear;




	public VOTrip(String trip_id, String start_time, String end_time, String bike_id, String tripDiuration,
			String fromStationId, String fromStationName, String toStationId, String toStationName, String userType,
			String gender, String birthYear) {
		super();
		this.trip_id = trip_id;
		this.start_time = start_time;
		this.end_time = end_time;
		this.bike_id = bike_id;
		this.tripDiuration = tripDiuration;
		this.fromStationId = fromStationId;
		this.fromStationName = fromStationName;
		this.toStationId = toStationId;
		this.toStationName = toStationName;
		this.userType = userType;
		this.gender = gender;
		this.birthYear = birthYear;
	}



	public String getTrip_id()
	{
		return trip_id;
	}

	public void setTrip_id(String trip_id)
	{
		this.trip_id = trip_id;
	}

	public String getStart_time() 
	{
		return start_time;
	}

	public void setStart_time(String start_time)
	{
		this.start_time = start_time;
	}

	public String getEnd_time() 
	{
		return end_time;
	}

	public void setEnd_time(String end_time) 
	{
		this.end_time = end_time;
	}

	public String getBike_id() 
	{
		return bike_id;
	}

	public void setBike_id(String bike_id) 
	{
		this.bike_id = bike_id;
	}

	public String getTripDiuration() 
	{
		return tripDiuration;
	}

	public void setTripDiuration(String tripDiuration) 
	{
		this.tripDiuration = tripDiuration;
	}

	public String getFromStationId() 
	{
		return fromStationId;
	}

	public void setFromStationId(String fromStationId)
	{
		this.fromStationId = fromStationId;
	}

	public String getFromStationName() 
	{
		return fromStationName;
	}

	public void setFromStationName(String fromStationName)
	{
		this.fromStationName = fromStationName;
	}

	public String getToStationId()
	{
		return toStationId;
	}

	public void setToStationId(String toStationId)
	{
		this.toStationId = toStationId;
	}

	public String getToStationName() 
	{
		return toStationName;
	}

	public void setToStationName(String toStationName)
	{
		this.toStationName = toStationName;
	}

	public String getUserType() 
	{
		return userType;
	}

	public void setUserType(String userType)
	{
		this.userType = userType;
	}

	public String getGender() 
	{
		return gender;
	}

	public void setGender(String gender) 
	{
		this.gender = gender;
	}

	public String getBirthYear() 
	{
		return birthYear;
	}

	public void setBirthYear(String birthYear) 
	{
		this.birthYear = birthYear;
	}



	@Override
	public int compareTo(VOTrip o) {
		// TODO Auto-generated method stub
		return 0;
	}
}
