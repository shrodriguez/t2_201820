package model.logic;

import java.io.FileReader;
import java.util.Iterator;

import com.opencsv.CSVReader;

import api.IDivvyTripsManager;
import model.vo.VOByke;
import model.vo.VOTrip;
import model.data_structures.DoubleLinkedList;
import model.data_structures.DoublyLinkedList;

public class DivvyTripsManager implements IDivvyTripsManager {

	private DoubleLinkedList<VOTrip> listTrip;
	private DoubleLinkedList<VOByke> listByke;



	public DivvyTripsManager() 
	{
		listTrip= new DoubleLinkedList<VOTrip>();
		listByke= new DoubleLinkedList<VOByke>(); 
	}


	public void loadStations (String stationsFile) 
	{
		try
		{
			CSVReader reader = new CSVReader(new FileReader(stationsFile));
			String [] nextLine;
			reader.readNext();
			while ((nextLine = reader.readNext()) != null) 
			{
				// nextLine[] is an array of values from the line
				VOByke station = new VOByke(nextLine[0], nextLine[1], nextLine[2], nextLine[3], nextLine[4], nextLine[5], nextLine[6]);
				
			}
		}
		catch(Exception e)
		{
			System.out.println("No se pudo cargar.");
			e.printStackTrace();
		}
	}


	public void loadTrips (String tripsFile) 
	{
		try
		{
			CSVReader reader = new CSVReader(new FileReader(tripsFile));
			String [] nextLine;
			reader.readNext();
			
			while ((nextLine = reader.readNext()) != null) 
			{
				// nextLine[] is an array of values from the line
				VOTrip trip = new VOTrip(nextLine[0], nextLine[1], nextLine[2], nextLine[3], nextLine[4], nextLine[5], nextLine[6], nextLine[7], nextLine[8], nextLine[9], nextLine[10], (nextLine[11]));
				listTrip.addFirst(trip);
			}
			
			System.out.println(listTrip.getSize());
		}
		catch(Exception e)
		{

			System.out.println("No se pudo cargar.");
			e.printStackTrace();
		}

	}

	@Override
	public DoublyLinkedList <VOTrip> getTripsOfGender (String gender) 
	{
		DoubleLinkedList<VOTrip> genderTrip = new DoubleLinkedList<>();
		Iterator<VOTrip> iter= listTrip.iterator();
		while(iter.hasNext())
		{
			VOTrip trip= iter.next();
			if(trip.getGender().equals(gender))
			{
				genderTrip.addFirst(trip);
			}
		}
		return genderTrip;
	}

	@Override
	public DoublyLinkedList <VOTrip> getTripsToStation (int stationID) 
	{
		DoubleLinkedList<VOTrip> stationTrip = new DoubleLinkedList<>();
		Iterator<VOTrip> iter= listTrip.iterator();
		while(iter.hasNext())
		{
			VOTrip trip= iter.next();
			if(Integer.parseInt(trip.getToStationId())== stationID)
			{
				stationTrip.addFirst(trip);
			}
		}
		return stationTrip;
	}	


}
