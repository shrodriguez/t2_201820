package model.data_structures;

public class Nodo<T>
{
	//---------------------------------------------------------------
	//Atributos
	//---------------------------------------------------------------
	private T element;
	private Nodo<T> anterior;
	private Nodo<T> siguiente;
	//---------------------------------------------------------------
	//Constructor
	//---------------------------------------------------------------
	public Nodo(T element) {
		this.element = element;
		this.anterior = null;
		this.siguiente = null;
	}
	//---------------------------------------------------------------
	//Metodos
	//---------------------------------------------------------------
	/**
	 * @return the element
	 */
	public T getElement() {
		return element;
	}
	/**
	 * @param element the element to set
	 */
	public void setElement(T element) {
		this.element = element;
	}
	/**
	 * @return the primero
	 */
	public Nodo<T> getAnterior() {
		return anterior;
	}
	/**
	 * @param primero the primero to set
	 */
	public void setAnterior(Nodo<T> primero) {
		this.anterior = primero;
	}
	/**
	 * @return the siguiente
	 */
	public Nodo<T> getSiguiente() {
		return siguiente;
	}
	/**
	 * @param siguiente the siguiente to set
	 */
	public void setSiguiente(Nodo<T> siguiente) {
		this.siguiente = siguiente;
	}




}
