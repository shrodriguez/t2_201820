package model.data_structures;

import java.util.Iterator;

public class DoubleLinkedList<T extends Comparable<T>> implements DoublyLinkedList<T>
{
	//---------------------------------------------------------------
	//Atributos
	//---------------------------------------------------------------
	private int size;
	private int posicion;
	private Nodo<T> primero;
	//---------------------------------------------------------------
	//Metodos
	//---------------------------------------------------------------
	public DoubleLinkedList(T element)
	{
		size=1;
		primero=new Nodo<T>(element);
		posicion=0;

	}
	public DoubleLinkedList()
	{
		size=0;
		primero=null;
		posicion=-1;
	}
	//---------------------------------------------------------------
	//Metodos
	//---------------------------------------------------------------
	@Override
	public Iterator<T> iterator()
	{
		return new Iterator<T>()
		{
			Nodo<T> act=null;
			public boolean hasNext()
			{
				if(size==0) return false;
				if(act==null) return true;
				return act.getSiguiente() !=null;
			}
			public T next() 
			{
				if(act==null)
					act= primero;
				else
					act=act.getSiguiente();
				return act.getElement();
			}
		};
	}

	@Override
	public boolean addFirst(T element) {
		boolean agrego= false;
		Nodo<T> nodoCreado= new Nodo(element); 
		if(primero==null)
		{
			primero = nodoCreado;
			size++;
			agrego=true;
		}
		else
		{
			nodoCreado.setSiguiente(primero);
			primero.setAnterior(nodoCreado);
			primero = nodoCreado;
			agrego=true;
			size++;
		}
		

		return agrego;
	}

	@Override
	public boolean add(T element) {
		boolean agrego= false;
		Nodo<T> nuevoNodo= new Nodo<T>(element);
		if(primero==null)
		{
			primero=nuevoNodo;
		}
		else{
			try{
				Nodo<T> act=primero;
				while(act.getSiguiente() !=null)
				{
					act=act.getSiguiente();
				}
				act.setSiguiente(nuevoNodo);
				nuevoNodo.setAnterior(act);
				agrego= true;
			}
			catch(Exception e)
			{

			}
		}
		if(agrego)size++;
		return agrego;

	}

	public boolean addAtk(T element, int k) {
		boolean agrego=false;
		int cont=0;
		Nodo<T> actual= primero;
		while(cont!=k && actual.getSiguiente()!=null)
		{
			cont++;
			actual=actual.getSiguiente();
		}
		Nodo <T> nuevo= new Nodo<T>(element);
		Nodo<T> anterior= actual.getAnterior();
		anterior.setSiguiente(nuevo);
		nuevo.setAnterior(anterior);
		nuevo.setSiguiente(actual);
		actual.setAnterior(nuevo);
		size++;
		agrego= true;

		return agrego;
	}

	public T getElement(int k) {
		Nodo<T> actual= primero;
		T element= null;
		int cont=0;
		while(cont!=k && actual.getSiguiente()!= null)
		{
			cont ++;
			actual=actual.getSiguiente();
		}
		element= actual.getElement();

		return element;
	}


	@Override
	public Integer getSize() {
		return size;
	}

	@Override
	public boolean delete(T element){
		boolean elimino=false;
		try
		{
			Nodo<T> nuevo=new Nodo<T>(element);
			if(primero.getElement().equals(element))
			{
				primero= primero.getSiguiente();
				primero.setAnterior(null);
				elimino= true;
			}
			else
			{
				Nodo<T> actualizar= primero;
				while(actualizar.getSiguiente() !=null && !elimino)
				{
					if(primero.getElement().equals(element) && actualizar.getSiguiente() != null)
					{
						Nodo<T> siguiente= actualizar.getSiguiente();
						Nodo<T> anterior = actualizar.getAnterior();
						siguiente.setAnterior(anterior);
						anterior.setSiguiente(siguiente);
						elimino= true;
					}
					else
					{
						if(primero.getElement().equals(element) && actualizar.getSiguiente()== null)
						{
							actualizar= actualizar.getAnterior();
							actualizar.setSiguiente(null);
							elimino= true;
						}
					}
				}
			}
		}
		catch(Exception e)
		{
		}

		return elimino;
	}

	@Override
	public boolean deleteAtK(int k) {
		boolean elimino= false;
		int cont= 0;
		Nodo<T> actual= primero;
		if(cont==k)
		{
			primero= primero.getSiguiente();
			primero.setAnterior(null);
		}
		else
		{
			while(cont!=k && actual.getSiguiente()!=null)
			{
				cont++;
				actual=actual.getSiguiente();
			}
			if(cont==size)
			{
				Nodo<T> anterior= actual.getAnterior();
				anterior.setSiguiente(null);
			}
			else
			{
				Nodo<T>anterior = actual.getAnterior();
				Nodo<T> siguiente = actual.getSiguiente();
				anterior.setSiguiente(siguiente);
				siguiente.setAnterior(anterior);		
			}
		}
		size--;
		elimino= true;

		return elimino;
	}


	//public T next(){ 

	//return null;
	//}


	//public T previous() {

	//return null;
	//}


}
