package controller;

import api.IDivvyTripsManager;
import model.data_structures.DoublyLinkedList;
import model.logic.DivvyTripsManager;
import model.vo.VOTrip;

public class Controller
{
	public final static String RUTE_STATION= "./src/data/Divvy_Stations_2017_Q3Q4.csv";
	public final static String RUTE_TRIPS= "./src/data/Divvy_Trips_2017_Q4.csv";


	/**
	 * Reference to the services manager
	 */
	@SuppressWarnings("unused")
	private static IDivvyTripsManager  manager = new DivvyTripsManager();

	public static void loadStations() 
	{
		manager.loadStations(RUTE_STATION);
	}

	public static void loadTrips()
	{
		manager.loadTrips(RUTE_TRIPS);
	}

	public static DoublyLinkedList <VOTrip> getTripsOfGender (String gender) {
		return manager.getTripsOfGender(gender);
	}

	public static DoublyLinkedList <VOTrip> getTripsToStation (int stationID) {
		return manager.getTripsToStation(stationID);
	}
}
